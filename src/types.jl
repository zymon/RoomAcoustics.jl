
export AbstractRoom, RectangularRoom
export AbstractRIRConfig, ISMConfig
export Node, Event


abstract type AbstractRoom end

struct RectangularRoom{T<:Real} <: AbstractRoom
    c::T                    # Sound wave velocity
    L::Tuple{T,T,T}         # size of room (x, y, z)
    β::Tuple{T,T,T,T,T,T}   # absorption coeffictions for walls
end



abstract type AbstractRIRConfig end

"""

"""
@kwdef struct ISMConfig{T<:Real,I<:Integer,R<:AbstractRNG} <: AbstractRIRConfig
    order::Tuple{I,I} = (0, -1) # Order of reflection [low, high]
    fs::T = 16e3                # Sampling frequency
    N::I  = 4000                # Number of samples in impulse response
    Wd::T = 8e-3                # Single impulse width
    hp::Bool = true             # High pass filter
    isd::T = 0.0                # Image source distortion (randomized image method)
    lrng::R = GLOBAL_RNG        # Local randon number generator
end

struct Node{T<:Real}
    rx::TxRxArray{T}
    fs::T
    δ::T
    function Node(rx, fs::T, δ::T = 0.0) where T
        δ < 0.0 && error("δ < 0")
        fs < 0.0 && error("fs < 0")
        new{T}(rx, fs, δ)
    end
end

struct Event{T<:Real, V<:AbstractVector{<:T}} # NOTE: TxNode może będzie lepszą nazwa?
    tx::TxRx{T}
    emission::T
    fs::T
    signal::V
    function Event(tx, emission::T, fs::T, signal::V) where {T, V}
        emission < 0.0 && error("emission < 0")
        fs < 0.0 && error("fs < 0")
        new{T, V}(tx, emission, fs, signal)
    end
end
